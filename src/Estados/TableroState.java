
public interface TableroState {

	public void start(Tablero t);
	public void input(Tablero t);
	public void waits(Tablero t);
	public void win(Tablero t);
	
}
