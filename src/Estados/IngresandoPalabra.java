import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class IngresandoPalabra implements TableroState{

	@Override
	public void start(Tablero t) {
		// TODO Auto-generated method stub
		t.setCurrentState(t.getIngresando());
		
	}

	@Override
	public void input(Tablero t) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
	  	System.out.println("Mago Roberto, ingresa la palabra secreta: ");

	    
	  	String  palabrasecreta = in.nextLine();
	  	t.setPalabraSecreta(palabrasecreta);
	  	
	  	if(palabrasecreta != null){
	  		t.setCurrentState(t.getTurno1());
	  	}
		
	}

	@Override
	public void waits(Tablero t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void win(Tablero t) {
		// TODO Auto-generated method stub
		
	}

	
}
