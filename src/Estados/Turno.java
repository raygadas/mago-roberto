import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Turno implements TableroState {
	@Override
	public void start(Tablero t) {
		// TODO Auto-generated method stub
		t.setCurrentState(t.getIngresando());
	}

	@Override
	public void input(Tablero t) {
		// TODO Auto-generated method stub
	}

	@Override
	public void waits(Tablero t) {
		// TODO Auto-generated method stub
		System.out.println("Cambio de turno.");

	  	if(t.getCurrentState().equals(t.getTurno1())){
			t.setCurrentState(t.getTurno2());
		  	System.out.println("Turno Jugador 2: ");
		}

		else if(t.getCurrentState().equals(t.getTurno2())){
			t.setCurrentState(t.getTurno1());
		  	System.out.println("Turno jugador 1: ");
		}
		
	}
	
	

	@Override
	public void win(Tablero t) {
		// TODO Auto-generated method stub
		
		showblack(t);
		
	  	System.out.println("Turno jugador 1:");

		do {
		Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));

	  	String  s = in.nextLine();
	  	t.setIntento(s);
	  	
		if(t.getIntento().equals(t.getPalabraSecreta())){
			if(t.getCurrentState().equals(t.getTurno1())){
			  	System.out.println("Felicidades J1:");
			}

			else if(t.getCurrentState().equals(t.getTurno2())){
			  	System.out.println("Turno jugador 2:");
			}

		  	t.setWin(true);
			t.setCurrentState(t.getFin());
		}
		
		else if(t.getPalabraSecreta().contains(s.substring(0, 1))){
			
			char prueba = s.substring(0, 1).charAt(0);
			char [] ciclo = t.getPalabraSecreta().toCharArray();
			char [] show = t.getMuestra().toCharArray();
			
			for(int i =0; i< t.getMuestra().length(); i++){
				if(show[i]=='*'){
					if (prueba == ciclo[i]){
						show[i]=prueba;
					}
				}
			}
			
			String nueva="";
			for(int i =0; i< t.getMuestra().length(); i++){
				nueva=nueva+show[i];
			}

			t.setMuestra(nueva);
			
			System.out.println(nueva);
			
			if (nueva.equals(t.getPalabraSecreta())){

				if(t.getCurrentState().equals(t.getTurno1())){
				  	System.out.println("Felicidades J1:");
				}

				else if(t.getCurrentState().equals(t.getTurno2())){
				  	System.out.println("Turno Jugador 2:");
				}
				
				t.setWin(true);

				t.setCurrentState(t.getFin());
				
			}
		}
		

		else{
		  waits(t);
		}

		}while (t.isWin()==false);
		
	}
	
	
	public void showblack(Tablero t){
		String show="";
		for(int i =0; i< t.getPalabraSecreta().length(); i++){
			show=show+"*";
		}
		t.setMuestra(show);
		System.out.println(show);
	
	}
	
	
	
	
	
	
	
}
