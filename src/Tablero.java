
public class Tablero  {
	
	public String intento;
	public String palabrasecreta;
	public String muestra;

	
	public TableroState ingresando = new IngresandoPalabra();
	public TableroState turno1 = new Turno();
	public TableroState turno2 = new Turno();
	public TableroState fin = new Finalizado();
	public TableroState currentState;
	public boolean win;

	
	public Tablero() {
		this.intento = "";
		this.palabrasecreta = "";
		this.currentState = ingresando;
		this.win = false;
	}

	public void start() {
		// TODO Auto-generated method stub
		currentState.start(this);
		
	}

	public void input() {
		// TODO Auto-generated method stub
		currentState.input(this);
	}

	public void waits() {
		// TODO Auto-generated method stub
		currentState.waits(this);
		
	}

	public void win() {
		// TODO Auto-generated method stub
		currentState.win(this);
	}

	public String getIntento() {
		return intento;
	}

	public String getPalabraSecreta() {
		return palabrasecreta;
	}

	public TableroState getIngresando() {
		return ingresando;
	}

	public TableroState getTurno1() {
		return turno1;
	}

	public TableroState getTurno2() {
		return turno2;
	}

	public TableroState getFin() {
		return fin;
	}

	public TableroState getCurrentState() {
		return currentState;
	}

	public void setIntento(String intento) {
		this.intento = intento;
	}

	public void setPalabraSecreta(String palabrasecreta) {
		this.palabrasecreta = palabrasecreta;
	}

	public void setIngresando(TableroState ingresando) {
		this.ingresando = ingresando;
	}

	public void setTurno1(TableroState turno1) {
		this.turno1 = turno1;
	}

	public void setTurno2(TableroState turno2) {
		this.turno2 = turno2;
	}

	public void setFin(TableroState fin) {
		this.fin = fin;
	}

	public void setCurrentState(TableroState currentState) {
		this.currentState = currentState;
	}

	public boolean isWin() {
		return win;
	}

	public void setWin(boolean win) {
		this.win = win;
	}

	public String getMuestra() {
		return muestra;
	}

	public void setMuestra(String muestra) {
		this.muestra = muestra;
	}
	
	public void run(){
		start();
		input();
		win();
		win();
	}
}
